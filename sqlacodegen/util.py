
import sys
import re
from keyword import iskeyword

from inspect import ArgSpec
import inspect

from sqlalchemy.util import OrderedDict
from sqlalchemy import (Enum, ForeignKeyConstraint, PrimaryKeyConstraint,
        CheckConstraint, UniqueConstraint, Table, Column, Sequence)
from sqlalchemy.schema import ForeignKey
from sqlalchemy.util import OrderedDict
from sqlalchemy.types import Boolean, String
import sqlalchemy

try:
    from sqlalchemy.sql.expression import text, TextClause
except ImportError:
    # SQLAlchemy < 0.8
    from sqlalchemy.sql.expression import text, _TextClause as TextClause

_re_boolean_check_constraint = re.compile(r"(?:(?:.*?)\.)?(.*?) IN \(0, 1\)")
_re_column_name = re.compile(r'(?:(["`]?)(?:.*)\1\.)?(["`]?)(.*)\2')
_re_enum_check_constraint = re.compile(r"(?:(?:.*?)\.)?(.*?) IN \((.+)\)")
_re_enum_item = re.compile(r"'(.*?)(?<!\\)'")
_re_invalid_identifier = re.compile(r'[^a-zA-Z0-9_]' if sys.version_info[0] < 3 else r'(?u)\W')
_re_seq_nextval = re.compile(r"nextval\('([^']*)'[^)]*\)")

class DummyInflectEngine(object):
    def singular_noun(self, noun):
        return noun

def convert_to_valid_identifier(name):
    assert name, 'Identifier cannot be empty'
    if name[0].isdigit() or iskeyword(name):
        name = '_' + name
    return _re_invalid_identifier.sub('_', name)


def get_compiled_expression(statement):
    """Returns the statement in a form where any placeholders have been filled in."""
    if isinstance(statement, TextClause):
        return statement.text

    dialect = statement._from_objects[0].bind.dialect
    compiler = statement._compiler(dialect)

    # Adapted from http://stackoverflow.com/a/5698357/242021
    class LiteralCompiler(compiler.__class__):
        def visit_bindparam(self, bindparam, within_columns_clause=False, literal_binds=False, **kwargs):
            return super(LiteralCompiler, self).render_literal_bindparam(
                bindparam, within_columns_clause=within_columns_clause,
                literal_binds=literal_binds, **kwargs
            )

    compiler = LiteralCompiler(dialect, statement)
    return compiler.process(statement)


def get_constraint_sort_key(constraint):
    if isinstance(constraint, CheckConstraint):
        return 'C{0}'.format(constraint.sqltext)
    return constraint.__class__.__name__[0] + repr(constraint.columns)


def get_common_fk_constraints(table1, table2):
    """Returns a set of foreign key constraints the two tables have against each other."""
    c1 = set(c for c in table1.constraints if isinstance(c, ForeignKeyConstraint) and
             c.elements[0].column.table == table2)
    c2 = set(c for c in table2.constraints if isinstance(c, ForeignKeyConstraint) and
             c.elements[0].column.table == table1)
    return c1.union(c2)


def getargspec_init(method):
    try:
        return inspect.getargspec(method)
    except TypeError:
        if method is object.__init__:
            return ArgSpec(['self'], None, None, None)
        else:
            return ArgSpec(['self'], 'args', 'kwargs', None)



class ImportCollector(OrderedDict):
    def add_import(self, obj):
        type_ = type(obj) if not isinstance(obj, type) else obj
        pkgname = 'sqlalchemy' if type_.__name__ in sqlalchemy.__all__ else type_.__module__
        self.add_literal_import(pkgname, type_.__name__)

    def add_literal_import(self, pkgname, name):
        names = self.setdefault(pkgname, set())
        names.add(name)

    def render(self):
        return '\n'.join('from {0} import {1}'.format(package, ', '.join(sorted(names)))
                         for package, names in self.items())


class AbstractContext(object):
    def __init__(self):
        # Regexes - turn them into functions later
        self.re_boolean_check_constraint = _re_boolean_check_constraint
        self.re_column_name = _re_column_name
        self.re_enum_check_constraint = _re_enum_check_constraint
        self.re_enum_item = _re_enum_item
        self.re_invalid_identifier = _re_invalid_identifier
        # "Utility" functions
        self.convert_to_valid_identifier = convert_to_valid_identifier
        self.get_compiled_expression = get_compiled_expression
        self.get_constraint_sort_key = get_constraint_sort_key
        self.get_common_fk_constraints = get_common_fk_constraints
        self.getargspec_init = getargspec_init
        # Objects 
        self.inflect_engine = DummyInflectEngine()
        self.import_collector = ImportCollector()
        # New settings
        # Support for Alembic and sqlalchemy-migrate -- never expose the schema version tables
        self.tables_excluded = ('alembic_version', 'migrate_version')

    def include_table(self, table):
        return not (table.name in self.tables_excluded)

    def use_declarative_style(self, table):
        return True

    def render_server_default(self, table, column):
        default_expr = self.get_compiled_expression(column.server_default.arg)
        # Recognize PostgreSQL sequences: text("nextval('celltower_id_seq'::regclass)")
        m = _re_seq_nextval.match(default_expr)
        if m:
            return "default=Sequence('{0}')".format(m.group(1))
        elif '\n' in default_expr:
            return 'server_default=text("""\\\n{0}""")'.format(default_expr)
        else:
            return 'server_default=text("{0}")'.format(default_expr)


    def render_column_type(self, coltype):
        args = []
        if isinstance(coltype, Enum):
            args.extend(repr(arg) for arg in coltype.enums)
            if coltype.name is not None:
                args.append('name={0!r}'.format(coltype.name))
        else:
            # All other types
            argspec = self.getargspec_init(coltype.__class__.__init__)
            defaults = dict(zip(argspec.args[-len(argspec.defaults or ()):], argspec.defaults or ()))
            missing = object()
            use_kwargs = False
            for attr in argspec.args[1:]:
                # Remove annoyances like _warn_on_bytestring
                if attr.startswith('_'):
                    continue

                value = getattr(coltype, attr, missing)
                default = defaults.get(attr, missing)
                if value is missing or value == default:
                    use_kwargs = True
                elif use_kwargs:
                    args.append('{0}={1}'.format(attr, repr(value)))
                else:
                    args.append(repr(value))

        text = coltype.__class__.__name__
        if args:
            text += '({0})'.format(', '.join(args))

        return text


    def render_column(self, table, column, show_name):
        kwarg = []
        is_sole_pk = column.primary_key and len(column.table.primary_key) == 1
        dedicated_fks = [c for c in column.foreign_keys if len(c.constraint.columns) == 1]
        is_unique = any(isinstance(c, UniqueConstraint) and set(c.columns) == set([column])
                        for c in column.table.constraints)
        is_unique = is_unique or any(i.unique and set(i.columns) == set([column]) for i in column.table.indexes)
        has_index = any(set(i.columns) == set([column]) for i in column.table.indexes)

        # Render the column type if there are no foreign keys on it or any of them points back to itself
        render_coltype = not dedicated_fks or any(fk.column is column for fk in dedicated_fks)

        if column.key != column.name:
            kwarg.append('key')
        if column.primary_key:
            kwarg.append('primary_key')
        if not column.nullable and not is_sole_pk:
            kwarg.append('nullable')
        if is_unique:
            column.unique = True
            kwarg.append('unique')
        elif has_index:
            column.index = True
            kwarg.append('index')
        if column.server_default:
            server_default = self.render_server_default(table, column)
        
        return 'Column({0})'.format(', '.join(
            ([repr(column.name)] if show_name else []) +
            ([self.render_column_type(column.type)] if render_coltype else []) +
            [self.render_constraint(x) for x in dedicated_fks] +
            [repr(x) for x in column.constraints] +
            ['{0}={1}'.format(k, repr(getattr(column, k))) for k in kwarg] +
            ([server_default] if column.server_default else [])
        ))

    def render_fk_options(self, constraint, *opts):
        opts = [repr(opt) for opt in opts]
        for attr in 'ondelete', 'onupdate', 'deferrable', 'initially', 'match':
            value = getattr(constraint, attr, None)
            if value:
                opts.append('{0}={1!r}'.format(attr, value))

        return ', '.join(opts)

    def render_constraint(self, constraint):
        if isinstance(constraint, ForeignKey):
            remote_column = '{0}.{1}'.format(constraint.column.table.fullname, constraint.column.name)
            return 'ForeignKey({0})'.format(self.render_fk_options(constraint, remote_column))
        elif isinstance(constraint, ForeignKeyConstraint):
            local_columns = constraint.columns
            remote_columns = ['{0}.{1}'.format(fk.column.table.fullname, fk.column.name)
                              for fk in constraint.elements]
            return 'ForeignKeyConstraint({0})'.format(self.render_fk_options(constraint, local_columns, remote_columns))
        elif isinstance(constraint, CheckConstraint):
            return 'CheckConstraint({0!r})'.format(_get_compiled_expression(constraint.sqltext))
        elif isinstance(constraint, UniqueConstraint):
            columns = [repr(col.name) for col in constraint.columns]
            return 'UniqueConstraint({0})'.format(', '.join(columns))


    def render_index(self, index):
        columns = [repr(col.name) for col in index.columns]
        return 'Index({0!r}, {1})'.format(index.name, ', '.join(columns))

