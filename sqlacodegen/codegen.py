"""Contains the code generation logic and helper functions."""
from __future__ import unicode_literals, division, print_function, absolute_import
from collections import defaultdict

from sqlacodegen.util import *
import sqlacodegen.util as util


class Context(util.AbstractContext):
    def __init__(self, inflect_engine = None):
        super(Context, self).__init__()
        # Options
        self.views = True
        self.indices = True
        self.constraints = True
        self.joined = True
        # Objects
        if not inflect_engine is None:
            self.inflect_engine = inflect_engine()
        # (Model-)Classes
        self.Model = Model
        self.ModelTable = ModelTable
        self.ModelClass = ModelClass
        self.Relationship = Relationship
        self.ManyToOneRelationship = ManyToOneRelationship
        self.ManyToManyRelationship = ManyToManyRelationship
        # ...


class Model(object):
    def __init__(self, ctx, table):
        super(Model, self).__init__()
        self.table = table
        self.schema = table.schema

        # Adapt column types to the most reasonable generic types (ie. VARCHAR -> String)
        for column in table.columns:
            cls = column.type.__class__
            for supercls in cls.__mro__:
                if hasattr(supercls, '__visit_name__'):
                    cls = supercls
                if supercls.__name__ != supercls.__name__.upper() and not supercls.__name__.startswith('_'):
                    break

            column.type = column.type.adapt(cls)

    def add_imports(self, ctx):
        collector = ctx.import_collector
        if self.table.columns:
            collector.add_import(Column)

        for column in self.table.columns:
            collector.add_import(column.type)
            if column.server_default:
                collector.add_literal_import('sqlalchemy', 'text')

        for constraint in sorted(self.table.constraints, key=ctx.get_constraint_sort_key):
            if isinstance(constraint, ForeignKeyConstraint):
                if len(constraint.columns) > 1:
                    collector.add_literal_import('sqlalchemy', 'ForeignKeyConstraint')
                else:
                    collector.add_literal_import('sqlalchemy', 'ForeignKey')
            elif isinstance(constraint, UniqueConstraint):
                if len(constraint.columns) > 1:
                    collector.add_literal_import('sqlalchemy', 'UniqueConstraint')
            elif not isinstance(constraint, PrimaryKeyConstraint):
                collector.add_import(constraint)

        for index in self.table.indexes:
            if len(index.columns) > 1:
                collector.add_import(index)


class ModelTable(Model):
    def add_imports(self, ctx):
        super(ModelTable, self).add_imports(ctx)
        ctx.import_collector.add_import(Table)

    def render(self, ctx):
        text = 't_{0} = Table(\n    {0!r}, metadata,\n'.format(self.table.name)

        for column in self.table.columns:
            text += '    {0},\n'.format(ctx.render_column(self.table, column, True))

        for constraint in sorted(self.table.constraints, key=ctx.get_constraint_sort_key):
            if isinstance(constraint, PrimaryKeyConstraint):
                continue
            if isinstance(constraint, (ForeignKeyConstraint, UniqueConstraint)) and len(constraint.columns) == 1:
                continue
            text += '    {0},\n'.format(ctx.render_constraint(constraint))

        for index in self.table.indexes:
            if len(index.columns) > 1:
                text += '    {0},\n'.format(ctx.render_index(index))

        if self.schema:
            text += "    schema='{0}',\n".format(self.schema)

        return text.rstrip('\n,') + '\n)'


class ModelClass(Model):
    parent_name = 'Base'

    def __init__(self, ctx, table, association_tables):
        super(ModelClass, self).__init__(ctx, table)
        self.name = self._tablename_to_classname(table.name, ctx.inflect_engine)
        self.children = []
        self.attributes = OrderedDict()

        # Assign attribute names for columns
        for column in table.columns:
            self._add_attribute(ctx, column.name, column)

        # Add many-to-one relationships
        pk_column_names = set(col.name for col in table.primary_key.columns)
        for constraint in sorted(table.constraints, key=ctx.get_constraint_sort_key):
            if isinstance(constraint, ForeignKeyConstraint):
                target_cls = self._tablename_to_classname(constraint.elements[0].column.table.name, ctx.inflect_engine)
                if ctx.joined and self.parent_name == 'Base' and set(constraint.columns) == pk_column_names:
                    self.parent_name = target_cls
                else:
                    relationship_ = ctx.ManyToOneRelationship(ctx, self.name, target_cls, constraint, ctx.inflect_engine)
                    self._add_attribute(ctx, relationship_.preferred_name, relationship_)

        # Add many-to-many relationships
        for association_table in association_tables:
            fk_constraints = [c for c in association_table.constraints if isinstance(c, ForeignKeyConstraint)]
            fk_constraints.sort(key=ctx.get_constraint_sort_key)
            target_cls = self._tablename_to_classname(fk_constraints[1].elements[0].column.table.name, ctx.inflect_engine)
            relationship_ = ctx.ManyToManyRelationship(ctx, self.name, target_cls, association_table)
            self._add_attribute(ctx, relationship_.preferred_name, relationship_)

    @staticmethod
    def _tablename_to_classname(tablename, inflect_engine):
        camel_case_name = ''.join(part.capitalize() for part in tablename.split('_'))
        return inflect_engine.singular_noun(camel_case_name) or camel_case_name

    def _add_attribute(self, ctx, attrname, value):
        attrname = tempname = ctx.convert_to_valid_identifier(attrname)
        counter = 1
        while tempname in self.attributes:
            tempname = attrname + str(counter)
            counter += 1

        self.attributes[tempname] = value
        return tempname

    def add_imports(self, ctx):
        super(ModelClass, self).add_imports(ctx)

        if any(isinstance(value, ctx.Relationship) for value in self.attributes.values()):
            ctx.import_collector.add_literal_import('sqlalchemy.orm', 'relationship')

        for child in self.children:
            child.add_imports(ctx)

    def render(self, ctx):
        text = 'class {0}({1}):\n'.format(self.name, self.parent_name)
        text += '    __tablename__ = {0!r}\n'.format(self.table.name)

        # Render constraints and indexes as __table_args__
        table_args = []
        for constraint in sorted(self.table.constraints, key=ctx.get_constraint_sort_key):
            if isinstance(constraint, PrimaryKeyConstraint):
                continue
            if isinstance(constraint, (ForeignKeyConstraint, UniqueConstraint)) and len(constraint.columns) == 1:
                continue
            table_args.append(ctx.render_constraint(constraint))
        for index in self.table.indexes:
            if len(index.columns) > 1:
                table_args.append(ctx.render_index(index))

        table_kwargs = {}
        if self.schema:
            table_kwargs['schema'] = self.schema

        kwargs_items = ', '.join('{0!r}: {1!r}'.format(key, table_kwargs[key]) for key in table_kwargs)
        kwargs_items = '{{{0}}}'.format(kwargs_items) if kwargs_items else None
        if table_kwargs and not table_args:
            text += '    __table_args__ = {0}\n'.format(kwargs_items)
        elif table_args:
            if kwargs_items:
                table_args.append(kwargs_items)
            if len(table_args) == 1:
                table_args[0] += ','
            text += '    __table_args__ = (\n        {0}\n    )\n'.format(',\n        '.join(table_args))

        # Render columns
        text += '\n'
        for attr, column in self.attributes.items():
            if isinstance(column, Column):
                show_name = attr != column.name
                text += '    {0} = {1}\n'.format(attr, ctx.render_column(self.table, column, show_name))

        # Render relationships
        if any(isinstance(value, Relationship) for value in self.attributes.values()):
            text += '\n'
        for attr, relationship in self.attributes.items():
            if isinstance(relationship, Relationship):
                text += '    {0} = {1}\n'.format(attr, relationship.render(ctx))

        # Render subclasses
        for child_class in self.children:
            text += '\n\n' + child_class.render(ctx)

        return text


class Relationship(object):
    def __init__(self, source_cls, target_cls):
        super(Relationship, self).__init__()
        self.source_cls = source_cls
        self.target_cls = target_cls
        self.kwargs = OrderedDict()

    def render(self, ctx):
        text = 'relationship('
        args = [repr(self.target_cls)]

        if 'secondaryjoin' in self.kwargs:
            text += '\n        '
            delimiter, end = ',\n        ', '\n    )'
        else:
            delimiter, end = ', ', ')'

        args.extend([key + '=' + value for key, value in self.kwargs.items()])
        return text + delimiter.join(args) + end


class ManyToOneRelationship(Relationship):
    def __init__(self, ctx, source_cls, target_cls, constraint, inflect_engine):
        super(ManyToOneRelationship, self).__init__(source_cls, target_cls)

        colname = constraint.columns[0]
        tablename = constraint.elements[0].column.table.name
        if not colname.endswith('_id'):
            self.preferred_name = inflect_engine.singular_noun(tablename) or tablename
        else:
            self.preferred_name = colname[:-3]

        # Add uselist=False to One-to-One relationships
        if any(isinstance(c, (PrimaryKeyConstraint, UniqueConstraint)) and
               set(col.name for col in c.columns) == set(constraint.columns)
               for c in constraint.table.constraints):
            self.kwargs['uselist'] = 'False'

        # Handle self referential relationships
        if source_cls == target_cls:
            self.preferred_name = 'parent' if not colname.endswith('_id') else colname[:-3]
            pk_col_names = [col.name for col in constraint.table.primary_key]
            self.kwargs['remote_side'] = '[{0}]'.format(', '.join(pk_col_names))

        # If the two tables share more than one foreign key constraint,
        # SQLAlchemy needs an explicit primaryjoin to figure out which column(s) to join with
        common_fk_constraints = ctx.get_common_fk_constraints(constraint.table, constraint.elements[0].column.table)
        if len(common_fk_constraints) > 1:
            self.kwargs['primaryjoin'] = "'{0}.{1} == {2}.{3}'".format(source_cls, constraint.columns[0], target_cls,
                                                                       constraint.elements[0].column.name)


class ManyToManyRelationship(Relationship):
    def __init__(self, ctx, source_cls, target_cls, assocation_table):
        super(ManyToManyRelationship, self).__init__(source_cls, target_cls)

        self.kwargs['secondary'] = repr(assocation_table.name)
        constraints = [c for c in assocation_table.constraints if isinstance(c, ForeignKeyConstraint)]
        constraints.sort(key=ctx.get_constraint_sort_key)
        colname = constraints[1].columns[0]
        tablename = constraints[1].elements[0].column.table.name
        self.preferred_name = tablename if not colname.endswith('_id') else colname[:-3] + 's'

        # Handle self referential relationships
        if source_cls == target_cls:
            self.preferred_name = 'parents' if not colname.endswith('_id') else colname[:-3] + 's'
            pri_pairs = zip(constraints[0].columns, constraints[0].elements)
            sec_pairs = zip(constraints[1].columns, constraints[1].elements)
            pri_joins = ['{0}.{1} == {2}.c.{3}'.format(source_cls, elem.column.name, assocation_table.name, col)
                         for col, elem in pri_pairs]
            sec_joins = ['{0}.{1} == {2}.c.{3}'.format(target_cls, elem.column.name, assocation_table.name, col)
                         for col, elem in sec_pairs]
            self.kwargs['primaryjoin'] = (
                repr('and_({0})'.format(', '.join(pri_joins))) if len(pri_joins) > 1 else repr(pri_joins[0]))
            self.kwargs['secondaryjoin'] = (
                repr('and_({0})'.format(', '.join(sec_joins))) if len(sec_joins) > 1 else repr(sec_joins[0]))


class CodeGenerator(object):
    header = '# coding: utf-8'
    footer = ''

    def __init__(self, context=None):
        super(CodeGenerator, self).__init__()
        if context is None:
            context = Context()
        self.context = context
        self.models = []

    def pickAssociationTables(self, metadata):
        ctx = self.context
        # Pick association tables from the metadata into their own set, don't process them normally
        self.links = defaultdict(lambda: [])
        self.association_tables = set()

        for table in metadata.tables.values():
            # Link tables have exactly two foreign key constraints and all columns are involved in them
            fk_constraints = [constr for constr in table.constraints if isinstance(constr, ForeignKeyConstraint)]
            if len(fk_constraints) == 2 and all(col.foreign_keys for col in table.columns):
                self.association_tables.add(table.name)
                tablename = sorted(fk_constraints, key=ctx.get_constraint_sort_key)[0].elements[0].column.table.name
                self.links[tablename].append(table)
    
    def processCheckConstraint(self, table, constraint, sqltext):
        ctx = self.context
        # Turn any integer-like column with a CheckConstraint like "column IN (0, 1)" into a Boolean
        match = ctx.re_boolean_check_constraint.match(sqltext)
        if match:
            colname = ctx.re_column_name.match(match.group(1)).group(3)
            table.constraints.remove(constraint)
            table.c[colname].type = Boolean()
            return

        # Turn any string-type column with a CheckConstraint like "column IN (...)" into an Enum
        match = ctx.re_enum_check_constraint.match(sqltext)
        if match:
            colname = ctx.re_column_name.match(match.group(1)).group(3)
            items = match.group(2)
            if isinstance(table.c[colname].type, String):
                table.constraints.remove(constraint)
                if not isinstance(table.c[colname].type, Enum):
                    options = ctx.re_enum_item.findall(items)
                    table.c[colname].type = Enum(*options, native_enum=False)
                return

    def processConstraints(self, table):
        ctx = self.context
        if not ctx.constraints:
            table.constraints = set([table.primary_key])
            table.foreign_keys.clear()
            for col in table.columns:
                col.foreign_keys.clear()
        else:
            # Detect check constraints for boolean and enum columns
            for constraint in table.constraints.copy():
                if isinstance(constraint, CheckConstraint):
                    sqltext = ctx.get_compiled_expression(constraint.sqltext)
                    self.processCheckConstraint(table, constraint, sqltext)


    def processTable(self, table):
        ctx = self.context
        if not ctx.include_table(table):
            return

        if not ctx.indices:
            table.indexes.clear()

        self.processConstraints(table)

        # Only form model classes for tables that have a primary key and are not association tables
        can_use_declarative = True
        if not table.primary_key or table.name in self.association_tables:
            can_use_declarative = False
        
        if can_use_declarative and ctx.use_declarative_style(table):
            model = ctx.ModelClass(ctx, table, self.links[table.name])
            self.classes[model.name] = model
        else:
            model = ctx.ModelTable(ctx, table)

        self.models.append(model)
        model.add_imports(ctx)

    def build(self, metadata): 
        ctx = self.context
        self.pickAssociationTables(metadata)

        # Iterate through the tables and create model classes when possible
        self.classes = {}
        for table in sorted(metadata.tables.values(), key=lambda t: (t.schema or '', t.name)):
            self.processTable(table)

        # Nest inherited classes in their superclasses to ensure proper ordering
        for model in self.classes.values():
            if model.parent_name != 'Base':
                self.classes[model.parent_name].children.append(model)
                self.models.remove(model)

        # Add either the MetaData or declarative_base import depending on whether there are mapped classes or not
        if not any(isinstance(model, ctx.ModelClass) for model in self.models):
            ctx.import_collector.add_literal_import('sqlalchemy', 'MetaData')
        else:
            ctx.import_collector.add_literal_import('sqlalchemy.ext.declarative', 'declarative_base')

    def render(self, outfile=sys.stdout):
        ctx = self.context
        print(self.header, file=outfile)

        # Render the collected imports
        print(ctx.import_collector.render() + '\n\n', file=outfile)

        if any(isinstance(model, ctx.ModelClass) for model in self.models):
            print('Base = declarative_base()\nmetadata = Base.metadata', file=outfile)
        else:
            print('metadata = MetaData()', file=outfile)

        # Render the model tables and classes
        for model in self.models:
            print('\n\n' + model.render(ctx).rstrip('\n'), file=outfile)

        if self.footer:
            print(self.footer, file=outfile)
        
